all: mrproper testaro

srcDir := ./src

testaro: exceptionHandler.o tester.o reader.o main.o regex.o
	gcc -o testaro exceptionHandler.o tester.o reader.o main.o regex.o -Wall -Wextra

exceptionHandler.o: $(srcDir)/exceptionHandler.c $(srcDir)/exceptionHandler.h
	gcc -o exceptionHandler.o -c $(srcDir)/exceptionHandler.c -Wall -Wextra

tester.o: $(srcDir)/tester.c $(srcDir)/tester.h
	gcc -o tester.o -c $(srcDir)/tester.c -Wall -Wextra

reader.o: $(srcDir)/reader.c $(srcDir)/reader.h
	gcc -o reader.o -c $(srcDir)/reader.c -Wall -Wextra

main.o: $(srcDir)/main.c $(srcDir)/main.h
	gcc -o main.o -c $(srcDir)/main.c -Wall -Wextra
	
regex.o: $(srcDir)/regex.c $(srcDir)/regex.h
	gcc -o regex.o -c $(srcDir)/regex.c -Wall -Wextra

clean:
	rm -rf *.o

mrproper: clean
	rm -rf testaro