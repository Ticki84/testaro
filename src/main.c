#include "main.h"
#include "reader.h"
#include "exceptionHandler.h"

int main(int argc, char *argv[]) {
    if (argc > 2) errPrint(1, "Too many arguments");
    if (argc < 2) errPrint(1, "Not enough argument, you must specify the path of the description file");
    readScriptFile(argv[1]);
    return 0;
}