#ifndef RS2021_CASPAR_JACQMIN_EXCEPTIONHANDLER_H
#define RS2021_CASPAR_JACQMIN_EXCEPTIONHANDLER_H

void errPrint(int errNo, const char *msg);

#endif //RS2021_CASPAR_JACQMIN_EXCEPTIONHANDLER_H
