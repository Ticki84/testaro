#ifndef MAIN_READER_H
#define MAIN_READER_H

char *inputBuffer;
char *checkBuffer;
void readScriptFile(char *filePath);
void inputNext(char *input);
void checkNext(char *output);

#endif //MAIN_READER_H
