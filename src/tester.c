#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

#include "reader.h"
#include "exceptionHandler.h"
#include "tester.h"
#include "regex.h"

#define BUFFERSIZE 65536

int timeout = 5;
int expectedReturn = -1;
int expectedSig = -1;

void processTest(char* arg, char* in, char* out, int lineCounter, int regex_flag){
    const char *prefix[3];
    prefix[0] = "cd ";
    prefix[1] = "export ";
    prefix[2] = "unset ";
    int cmd = -1;
    for (int i = 0; i < 3; i++) {
        int len = strlen(prefix[i]);
        if (strncmp(prefix[i], arg, len) == 0) {
            memmove(&arg[0], &arg[len], strlen(arg) - len + 1); // on récupère l'arg
            while (arg[strlen(arg) - 1] == '\n') arg[strlen(arg) - 1] = '\0'; // on enlève les \n
            cmd = i;
            break;
        }
    }
    if (cmd >= 0) {
        char *cmdArg = malloc((strlen(arg) + 6)*sizeof(char) );
        sprintf(cmdArg,"echo %s", arg);
        char *interp = forkExec(cmdArg, "", lineCounter); // on interprète pour pouvoir utiliser des variables d'environnements par exemple
        switch (cmd) {
            case 0:
            {
                if (chdir(interp)) {
                    char errBuffer[BUFFERSIZE] = "";
                    sprintf(errBuffer, "Error while going to directory %s (line %d)", interp, lineCounter);
                    errPrint(4, errBuffer);
                }
                break;
            }
            case 1:
            {
                int idx = -1;
                for (size_t i = 0; i < strlen(interp); i++) {
                    if (interp[i] == ' ' || interp[i] == '\t') memmove(&interp[i], &interp[i + 1], strlen(interp) - i); // on supprime les espaces etc.
                    if (interp[i] == '=') {
                        idx = i;

                    }
                }
                char name[BUFFERSIZE] = "", value[BUFFERSIZE] = "";
                strncpy(name, interp, idx);
                strncpy(value, interp + idx + 1, strlen(interp) - idx);
                setenv(name, value, 1);
                break;
            }
            case 2:
                unsetenv(interp);
                break;
            default:
                break;
        }
        //free(interp);
        return;
    }
    else {
        execTest(arg, in, out, lineCounter, regex_flag);
    }
}

void processSet(char* arg, int lineCounter){

    const char *prefix[4];
    prefix[0] = "set timeout ";
    prefix[1] = "expect signal ";
    prefix[2] = "expect return ";
	prefix[3] = "include ";
    int cmd = -1;
    for (int i = 0; i < 4; i++) {
        int len = strlen(prefix[i]);
        if (strncmp(prefix[i], arg, len) == 0) {
            memmove(&arg[0], &arg[len], strlen(arg) - len + 1); // on récupère l'arg
            while (arg[strlen(arg) - 1] == '\n') arg[strlen(arg) - 1] = '\0'; // on enlève les \n
            cmd = i;
            break;
        }
    }
    switch (cmd) {
        case 0:
            timeout = atoi(arg);
            return;
        case 1:
            if (expectedReturn >= 0) {
                expectedReturn = -1;
            }
            expectedSig = atoi(arg);
            return;
        case 2:
            if (expectedSig >= 0) {
                expectedSig = -1;
            }
            expectedReturn = atoi(arg);
            return;
		case 3:
			execInclude(arg,lineCounter);
			return;
        default: {
            char errBuffer[BUFFERSIZE] = "";
            sprintf(errBuffer, "Unknown command (line %d): %s", lineCounter, arg);
            errPrint(1, errBuffer);
        }
    }
}

char* forkExec(char* arg, char* in, int lineCounter) {

    int outfd[2];
    int infd[2];
    pipe(outfd); // pipe out: le père écrit, le fils lit
    pipe(infd); // pipe in: le fils écrit, le père lit

    char* outBuffer = malloc(sizeof(char));
    *outBuffer = 0;

    if (!fork()) {
        // Fils
        close(0); // on ferme l'entrée standard
        close(1); // on ferme la sortie standard

        dup2(outfd[0], 0); // l'entrée standard devient synonyme de la sortie du pipe out
        dup2(infd[1], 1); // la sortie standard devient synonyme de l'entrée du pipe in

        close(outfd[0]); // on ferme les pipes qu'on utilise plus dans le fils + envoi de l'EOF
        close(outfd[1]);
        close(infd[0]);
        close(infd[1]);

        execlp("sh","sh","-c",arg,NULL); // on exécute la commande
        char errBuffer[BUFFERSIZE] = ""; // si on arrive là, l'exec n'a pas fonctionné
        sprintf(errBuffer, "Error while executing %s (line %d)", arg, lineCounter);
        errPrint(4, errBuffer);
    } else {
        // Père
        close(outfd[0]); // on ferme la sortie du pipe out
        close(infd[1]); // on ferme l'entrée du pipe in

        // On met en place l'alarme
        struct sigaction nvt,old;
        memset(&nvt, 0, sizeof(nvt));
        nvt.sa_handler = timedOut;
        sigaction(SIGALRM, &nvt, &old);
        alarm(timeout);

        write(outfd[1], in, strlen(in)); // on envoie l'accumulateur au fils
        close(outfd[1]); // on envoie l'EOF

        // Lecture de la sortie du fils
        char buffer[BUFFERSIZE] = "";
        while (read(infd[0], buffer, BUFFERSIZE)) { // lecture tant que le fils n'envoie pas l'EOF
            // On concatène à l'accumulateur
            char* tmp = realloc(outBuffer, sizeof(char) * (strlen(buffer) + strlen(outBuffer)) + 1);
            if (tmp) {
                outBuffer = tmp;
                strcat(outBuffer, buffer);
            }
            else errPrint(4, "Error while allocating buffer");
        }
        close(infd[0]); // on ferme la sortie du pipe in
        alarm(0); // On désactive l'alarme

        // Gestion des codes de retour et signaux
        int status;
        wait(&status); // on attend que le fils soit terminé (pour éviter les processus zombies)
        if (WIFEXITED(status)) {
            int exitStatus = WEXITSTATUS(status);
            if (expectedSig >= 0) { // Si on reçoit un exit code alors qu'on souhaitait un signal
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit signal %d (line %d) when exit signal %d was expected", exitStatus, lineCounter, expectedSig);
                errPrint(4 + exitStatus, exitStatusStr);
            }
            else if (expectedReturn != -1 && exitStatus != expectedReturn) { // Si on attend un exit code particulier mais qu'on en reçoit un autre
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit code %d (line %d) when exit code %d was expected", exitStatus, lineCounter, expectedReturn);
                errPrint(40 + exitStatus, exitStatusStr);
            }
            else if (expectedReturn == -1 && exitStatus != EXIT_SUCCESS) { // Si on reçoit un exit code sans en attendre un particulier
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit code %d (line %d)", exitStatus, lineCounter);
                errPrint(40 + exitStatus, exitStatusStr);
            }
        }
        else if (WIFSIGNALED(status)) {
            int exitStatus = WTERMSIG(status);
            if (expectedReturn >= 0) { // Si on reçoit un signal alors qu'on souhaitait un exit code
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit code %d (line %d) when exit code %d was expected", exitStatus, lineCounter, expectedReturn);
                errPrint(40 + exitStatus, exitStatusStr);
            }
            else if (expectedSig != -1 && exitStatus != expectedSig) { // Si on attend un signal particulier mais qu'on en reçoit un autre
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit signal %d (line %d) when signal %d was expected", exitStatus, lineCounter, expectedSig);
                errPrint(4 + exitStatus, exitStatusStr);
            }
            else if (expectedSig == -1 && exitStatus != SIGCHLD) { // Si on reçoit un signal sans en attendre un particulier
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Child finished with exit signal %d (line %d)", exitStatus, lineCounter);
                errPrint(4 + exitStatus, exitStatusStr);
            }
        }
        expectedReturn = -1;
        expectedSig = -1;
    }
    while (outBuffer[strlen(outBuffer) - 1] == '\n') outBuffer[strlen(outBuffer) - 1] = '\0'; // On supprime les \n
    return outBuffer;
}

void execTest(char* arg, char* in, char* out, int lineCounter,int regex_flag) {
    char* outBuffer = forkExec(arg, in, lineCounter);
    in[0] = '\0'; // on vide l'accumulateur

    while (out[strlen(out) - 1] == '\n') out[strlen(out) - 1] = '\0'; // on supprime les retours à la ligne
    // Comparaison de l'accumulateur avec la chaîne attendue
    if (regex_flag) {
        if (test_regex(outBuffer,out)==0){
            char errBuffer[BUFFERSIZE] = "";
            sprintf(errBuffer, "Unexpected output to command `%s` (line %d):\n%s\nExpected to match the pattern:\n%s", arg, lineCounter, outBuffer, out);
            errPrint(2, errBuffer);
        }
    } else { //Cas par défaut
        if (strcmp(outBuffer, out) != 0) {
            char errBuffer[BUFFERSIZE] = "";
            sprintf(errBuffer, "Unexpected output to command `%s` (line %d):\n%s\nExpected:\n%s", arg, lineCounter, outBuffer, out);
            errPrint(2, errBuffer);
        }
    }
    free(outBuffer);
    out[0] = '\0'; // on vide l'accumulateur
}

void execInclude(char* arg,int lineCounter){
	
    int infd[2];
    pipe(infd); // pipe in: le fils écrit, le père lit

    char* outBuffer = malloc(sizeof(char));
    *outBuffer = 0;

    if (!fork()) {
        // Fils
        dup2(infd[1], 2); // le canal d'erreur devient synonyme de l'entrée du pipe in
        
		close(infd[0]);
        close(infd[1]);

		readScriptFile(arg);
		exit(0);
    } else {
        // Père
        close(infd[1]); // on ferme l'entrée du pipe in

        // On met en place l'alarme
        struct sigaction nvt,old;
        memset(&nvt, 0, sizeof(nvt));
        nvt.sa_handler = timedOut;
        sigaction(SIGALRM, &nvt, &old);
        alarm(timeout);

        // Lecture de la sortie du fils
        char buffer[BUFFERSIZE] = "";
        while (read(infd[0], buffer, BUFFERSIZE)) { // lecture tant que le fils n'envoie pas l'EOF
            // On concatène à l'accumulateur
            char* tmp = realloc(outBuffer, sizeof(char) * (strlen(buffer) + strlen(outBuffer)) + 1);
            if (tmp) {
                outBuffer = tmp;
                strcat(outBuffer, buffer);
            }
            else errPrint(4, "Error while allocating buffer");
        }
        close(infd[0]); // on ferme la sortie du pipe in
        alarm(0); // On désactive l'alarme

        // Gestion des codes de retour et signaux
        int status;
        wait(&status); // on attend que le fils soit terminé (pour éviter les processus zombies)
        if (WIFEXITED(status)) {
            int exitStatus = WEXITSTATUS(status);
            if (exitStatus > 0) {
                char exitStatusStr[BUFFERSIZE] = "";
                sprintf(exitStatusStr, "Include %s finished with exit code %d (line %d):\n%s", arg, exitStatus, lineCounter, outBuffer);
                errPrint(exitStatus, exitStatusStr);
            }
        }
        else if (WIFSIGNALED(status)) {
            int exitStatus = WTERMSIG(status);
            char exitStatusStr[BUFFERSIZE] = "";
            sprintf(exitStatusStr, "Include called line %d \"%s\" finished with exit signal %d ", lineCounter, arg, exitStatus);
            errPrint(4 + exitStatus, exitStatusStr);
        }
    }
}

void timedOut() {
    errPrint(3, "Timed out");
}