//#ifndef RS2021_CASPAR_JACQMIN_EXECTEST_H
//#define RS2021_CASPAR_JACQMIN_EXECTEST_H

void processTest(char* arg, char* in, char* out, int lineCounter, int regex_flag);
void processSet(char* arg, int lineCounter);
void execTest(char* arg, char* in, char* out, int lineCounter, int regex_flag);
char* forkExec(char* arg, char* in, int lineCounter);
void timedOut();
void execInclude(char* arg,int lineCounter);

//#endif //RS2021_CASPAR_JACQMIN_EXCECTEST_H