#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

#include "regex.h"
#include "exceptionHandler.h"

int test_regex(char* sortie, char* reg){
	/*
		Teste si la chaîne de caractère donnée dans sortie correspond à l'expression régulière donnée dans reg
	*/
	
	int fd[2];
	pipe(fd);
	char* arg = malloc(sizeof(char)*(strlen(sortie)+strlen(reg)+27));
	if ( arg == NULL ){
		free(sortie); free(reg);
		errPrint(4, "Error while allocating buffer");
	}
	sprintf(arg,"perl -e 'print(\"%s\" =~ m/%s/)'",sortie,reg);

	if (fork()==0){ //fils
		
		dup2(fd[1],1); //La sortie standard doit être récupérée
		
		execlp("sh","sh","-c",arg,NULL);
		free(arg); free(sortie); free(reg);
        errPrint(4, "Error while testing regex"); //Cas où execlp ne fonctionne pas
	}
	
	close(fd[1]); //On ne veut pas écrire dans le pipe
	wait(NULL); //on attend la fin du fils
	
	char buffer[2] = {'0',0};
	
	read(fd[0],buffer,2);
	free(arg);
	if (buffer[0] == '1'){
		return 1;
	} else {
		return 0;
	}
}
