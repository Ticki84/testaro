#include <stdio.h>
#include <stdlib.h>
#include "exceptionHandler.h"

void errPrint(int errNo, const char *msg) {
    fprintf(stderr, "Error (code %i):\n%s\n", errNo, msg);
    exit(errNo);
}
