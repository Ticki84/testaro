#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "reader.h"
#include "exceptionHandler.h"
#include "tester.h"
#include "regex.h"

#define BUFFERSIZE 65536

void inputNext(char *input) {
    strcat(inputBuffer, input);
}

void checkNext(char *output) {
    strcat(checkBuffer, output);
}

void readScriptFile(char *filePath)
{
    inputBuffer = malloc(sizeof(char) * BUFFERSIZE);
	*inputBuffer = 0;
    checkBuffer = malloc(sizeof(char) * BUFFERSIZE);
	*checkBuffer = 0;
	int regex_flag = 0;

    FILE* fd = fopen(filePath, "r");
    if (!fd) {
        char errBuffer[BUFFERSIZE] = "";
        sprintf(errBuffer, "Can't open file %s", filePath);
        errPrint(1, errBuffer);
    }

    char *line = NULL;
    size_t len = 0;
    char *sLine = NULL; // si on doit lire une seconde ligne
    size_t sLen = 0;

    int lineCounter = 0;

    while(getline(&line, &len, fd) != -1) {
        lineCounter++;

        // Pour concaténer si un \ est en fin de phrase
        int i = strlen(line) - 1;
        while (i >= 0) {
            if (line[i] == '\n' || line[i] == '\t' || line[i] == ' ') {
                i--;
            }
            else if (line[i] == '\r') {
                memmove(&line[i], &line[i + 1], strlen(line) - i); // on supprime le retour chariot
                i--;
            }
            else if (line[i] == '\\') {
                if (i-1 > 0 && line[i-1] != '\\') {
                    if (getline(&sLine, &sLen, fd) != -1) {
                        lineCounter++;
                        line[i] = '\0';
                        char* tmp = realloc(line, sizeof(char) * (strlen(line) + strlen(sLine)) + 1);
                        if (tmp) {
                            line = tmp;
                            strcat(line, sLine);
                            i = strlen(line) - 1;
                            continue;
                        }
                        char errBuffer[BUFFERSIZE] = "";
                        sprintf(errBuffer, "Error while allocating buffer (line %d):\n%s", lineCounter, line);
                        errPrint(4, errBuffer);
                    }
                    char errBuffer[BUFFERSIZE] = "";
                    sprintf(errBuffer, "Syntax error, EOF after a \\ (line %d):\n%s", lineCounter, line);
                    errPrint(1, errBuffer);
                }
                else {
                    line[i] = '\0';
                    continue;
                }
            }
            else break;
        }
        if (i == -1) { // La ligne est vide
            continue;
        }

        // Lecture de la ligne
        if (line[0] != '\n' && line[0] != '#') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide) ou dans un commentaire
            i = 1;
            while (line[i] == ' ' || line[i] == '\t') i++; // si on est dans un espace on avance

            switch (line[0]) {
                case '$':
					processTest(line + i, inputBuffer, checkBuffer, lineCounter, regex_flag);
					regex_flag = 0;
					break;
                case '!':
                    processSet(line + i, lineCounter);
                    break;
                case '<':
                    inputNext(line + i);
                    break;
                case '>':
                    checkNext(line + i);
                    break;
                case 'p':
                    printf("%s", line + i);
                    break;
				case '~':
					checkNext(line + i);
					regex_flag = 1;
					break;
                default: {
                    char errBuffer[BUFFERSIZE] = "";
                    sprintf(errBuffer, "Unknown command (line %d):\n%s", lineCounter, line);
                    errPrint(1, errBuffer);
                    break;
                }
            }
        }
    }
    free(line);
    free(sLine);

    fclose(fd);

    free(inputBuffer);
    free(checkBuffer);
}